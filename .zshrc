# Lines configured by zsh-newuser-install
[[ $- != *i* ]] && return
HISTFILE=~/.cache/zsh/histfile
HISTSIZE=1000
SAVEHIST=1000
ZSH_CFG_DIR=$HOME/.config/zsh
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/hanss/.zshrc'
zstyle ':completion:*' menu yes select

unsetopt nomatch
autoload -Uz compinit
compinit
# End of lines added by compinstall
autoload -z edit-command-line
zle -N edit-command-line
bindkey -M vicmd v edit-command-line

export KEYTIMEOUT=1
export ZLE_RPROMPT_INDENT=0
export EDITOR=vim
export LC_ALL=en_US.UTF-8
export RUST_BACKTRACE=1
export PATH=$HOME/.local/bin:$PATH

alias dd='safer_dd'
alias sudo='sudo '
alias yay='yay --nosudoloop'
eval $(thefuck --alias 'F') 

#. /home/hanss/torch/install/bin/torch-activate

while read -r short c; do
    [ $short ] || continue
    export $short="$(eval "echo $c")"
done < $ZSH_CFG_DIR/zshexports

while read -r short c; do
    [ $short ] || continue
    alias $short="$c"
done < $ZSH_CFG_DIR/zshaliases

#source $ZSH_CFG_DIR/xdg-set

vi-append-x-selection () { RBUFFER=$(xsel -o -p </dev/null)$RBUFFER; }
vi-yank-x-selection () { print -rn -- $CUTBUFFER | xsel -i -p; }

zle -N vi-append-x-selection
bindkey -a 'p' vi-append-x-selection
zle -N vi-yank-x-selection
bindkey -a 'y' vi-yank-x-selection
(cat ~/.cache/wal/sequences &)

source ~/.cache/wal/colors-tty.sh
. $ZSH_CFG_DIR/zshfuncs
. $ZSH_CFG_DIR/zshprompt

source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins//zsh-autosuggestions/zsh-autosuggestions.zsh
