e=e(1)
pi=a(1)*4

define int(x) {
   auto s
   s = scale
   scale = 0
   x /= 1
   scale = s
   return (x)
}

define sin(x){ return s(x) }
define cos(x){ return c(x) }
define tan(x){ return s(x)/c(x) }
define sec(x){ return 1/c(x)}
define csc(x){ return 1/s(x)}
define cot(x){ return c(x)/s(x) }

define asin(x){
    if(x==1 || x==-1){ return x*pi/2 }
    return a(x / sqrt(1 - x^2))
}
define acos(x){
    if(x==0){ return pi/2 }
    return a(sqrt(1 - x^2) / x)
}
define atan(x){ return a(x) }
define asec(x){ return a(sqrt(x^2 - 1)) }
define acsc(x){
    if(x==1 || x==-1){ return x*pi/2 }
    return a(1/sqrt(x^2 - 1))
 }
define acot(x){ return pi/2 - a(x) }

define logb(b, x){ return l(x)/l(b) }
define ln(x){ return l(x) }
define pow(x, a) {
    if(a==int(a)) {return x^a}
    return e(a*l(x))
}

define fact(x){
    counter=1;
    for(i=2; i<=x; i++){
        counter *= i;
    }
    return counter;
}
