#!/usr/bin/env sh
killall -q polybar
# Launch bar1 and bar2
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

if type "xrandr"; then
  if [ "$(xrandr --query | grep " connected" | cut -d" " -f1 | wc -l)" = 2 ]; then
    polybar --reload secondary &
    polybar --reload main &
  
  else
    polybar --reload single &
  fi
else
  polybar --reload single &
fi


echo "Bars launched..."
