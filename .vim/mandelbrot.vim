function Fun(c,z)
   let nr = a:z[0]*a:z[0]-a:z[1]*a:z[1]
   let ni = 2*a:z[0]*a:z[1]
   return [nr+a:c[0],ni+a:c[1]]
endfunction

function Dist(z)
    return a:z[0]*a:z[0]+a:z[1]*a:z[1]
endfunction

function Escapes(c)
   let z=[0.0,0.0]
   let its=0
   while its < 50
       let z=Fun(a:c,z)
       if Dist(z) > 4
           return its
       endif
       let its = its+1
   endwhile
   return -1
endfunction

function DrawMandelbrot()
    let i = -1
    while l:i<1
        let r = -2.0
        let row = ""
        while l:r<0.5
            if Escapes([l:r,l:i]) < 0
                let row = row . "*"
            else
                let row = row . " "
            endif
            let r = l:r+0.02
        endwhile
        call append(line('$'), row) 
        let i = l:i+0.04
    endwhile
endfunction

command Mandelbrot call DrawMandelbrot()
" command -nargs=* Mandelbrot call DrawMandelbrot(<f-args>)
