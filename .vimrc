syntax on
set number relativenumber
runtime! archlinux.vim
"source ~/.vim/cobol.vim
source ~/.vim/mandelbrot.vim

if has("autocmd")
  autocmd BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$") | exe "normal! g`\"" | endif
  autocmd BufReadPost *.tex setl noai nocin nosi inde=
  autocmd BufWritePost *.r  !Rscript %
  autocmd BufWritePost *.tex !vimwritetex %
endif

let &t_EI .= "\<Esc>[2 q"
let &t_SI .= "\<Esc>[6 q"
call plug#begin('~/.vim/plugged')
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

Plug 'PotatoesMaster/i3-vim-syntax'
Plug 'rubixninja314/vim-mcfunction'
call plug#end()

set wildmode=longest,list,full
set background=dark
set splitbelow splitright
set shiftwidth=4
set autoindent
set smartindent
set expandtab
set tabstop=4


map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

nnoremap <C-P> :Find<CR>
